import bpy   
import time
import os
import subprocess

#On calcule le nombre de frames dans la scène + on le print dans la console pour être sûre que tout fonctionne ! 
def number_of_frames():
    num_frames = bpy.context.scene.frame_end - bpy.context.scene.frame_start + 1
    print("Number of frames in the scene:", num_frames)
    return num_frames

#On crée en arrière-plan un rendu de la première scène (qui peut être long si la scène est complexe et sur Cycle)
#+ Le résultat doit être en secondes. Nous en calculons le temps total du rendu entre le moment où il a commencé
#et le moment où il est terminé (+ print sur la console pour être sûr)
def first_render_time():
    original_frame = bpy.context.scene.frame_current
    bpy.context.scene.frame_set(1)

    start_time = time.time()
    bpy.ops.render.render(write_still=True)
    render_time = time.time() - start_time

    bpy.context.scene.frame_set(original_frame)

    print("Render time of the first frame:", round(render_time, 2), "seconds")

    return render_time


#La def pour éteindre l'ordinateur via l'os + print sur la console en cas de problème ! 
def shutdown_computer():
    try:
        subprocess.run(["shutdown", "/s", "/t", "1"], shell=True)
    except Exception as e:
        print("Error occurred while shutting down:", e)

#On crée la def signfier que si la case est cochée, on peut activer la def précédente pour éteindre l'ordinateur
#+ un print pour être rassurer, on n'en jamais assez ! 
def render_complete_handler(dummy):
    if bpy.context.scene.enable_shutdown == True:
        print("Render complete. Initiating shutdown.")
        shutdown_computer()

#Le calcul total des frames est fait en convertissant les données et heures, minutes et secondes et 
#en multipliant le nombre de frames par le temps de rendu d'une frame et on notre résultat final ! 
class MESH_OT_estimate_time(bpy.types.Operator):
    "Calculate Time"

    bl_idname = "calculate.estimate_time"
    bl_label = "estimate_time"

    def execute(self, context):
        bpy.context.scene.enable_shutdown = False
        #Si on calcule le temps estimé, le shutdown se désactive pour le pas éteindre l'ordinateur 
        #après le rendu de la première frame en arrière plan
        frames = number_of_frames()
        first_time = first_render_time()
        total_render_time = first_time * frames

        hours = int(total_render_time // 3600)
        minutes = int((total_render_time % 3600) // 60)
        seconds = int(total_render_time % 60)

        render_time_formatted = "{:02d}:{:02d}:{:02d}".format(hours, minutes, seconds) 

        print("Total estimated render time for all frames:", render_time_formatted)
        
        context.scene.render_time_formatted = render_time_formatted
        
        bpy.app.handlers.render_complete.remove(render_complete_handler)

        return {"FINISHED"}


#On crée le panel qui nous permet d'afficher les def/calculs pour l'utilisateur et qui peut déclencher les mécanismes
class MCN_PT_GM_SL(bpy.types.Panel):
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_label = "Estimated Time"
    bl_category = "Estimate Time Add-on"

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.operator("calculate.estimate_time", text="Calculate Time", icon="PREVIEW_RANGE")

        row = layout.row()
        row.label(text="Estimated Render Time: " + getattr(context.scene, "render_time_formatted", ""))

        row = layout.row()
        row.prop(context.scene, "enable_shutdown", text="Shutdown after render")

class MCN_PT_GM_SL_REDIRECT(bpy.types.Panel):
    """"""
    bl_label = "Render tools"
    bl_idname = "MCN_PT_GM_SL_REDIRECT"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"
    bl_parent_id = "MCN_PT_Main_Panel"
    bl_options = {'DEFAULT_CLOSED'}
    
    def draw(self, context):
        layout = self.layout
        layout.label(text="See GM_SL's Render tools in 'Estimate Time Add-on' category tab")


#On active/enregistre les class de l'add-on dans Blender ou non.
def register():
    bpy.utils.register_class(MCN_PT_GM_SL)
    bpy.utils.register_class(MCN_PT_GM_SL_REDIRECT)
    bpy.utils.register_class(MESH_OT_estimate_time)
    bpy.types.Scene.render_time_formatted = bpy.props.StringProperty()
    bpy.types.Scene.enable_shutdown = bpy.props.BoolProperty(name="Enable Shutdown", default=False)

    bpy.app.handlers.render_complete.append(render_complete_handler)

def unregister():
    bpy.utils.unregister_class(MCN_PT_GM_SL)
    bpy.utils.unregister_class(MCN_PT_GM_SL_REDIRECT)
    bpy.utils.unregister_class(MESH_OT_estimate_time)
    del bpy.types.Scene.render_time_formatted
    del bpy.types.Scene.enable_shutdown
    
#Sarah a fait les def pour éteindre l'ordinateur sous certaines conditions et celles pour calculer le temps
#Gautier a aidé pour les conversions et a fait le panel pour l'add-on.
    