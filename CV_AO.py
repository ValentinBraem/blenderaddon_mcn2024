import bpy

class MCN_PT_CV_AO(bpy.types.Panel):
    """"""
    bl_label = "Turn-around"
    bl_idname = "MCN_PT_CV_AO"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"
    bl_parent_id = "MCN_PT_Main_Panel"
    bl_options = {'DEFAULT_CLOSED'}
    
    def draw(self, context):
        
        layout = self.layout

        # TODO Replace this label by your stuff here
        layout.label(text="CV_AOs turn-around tools")
        
        # Informative Text
        layout.label(text="Remember to select your object!")
        
        # Buttons
        row = self.layout.row()
        row.operator("object.rotate_around", text="Complete Turn", icon="CAMERA_DATA").back_and_forth = False
        row.operator("object.rotate_around", text="Back and Forth", icon="CAMERA_DATA").back_and_forth = True
             
        # Interpolation mode Button
        row = self.layout.row()
        row.operator("object.set_interpolation_mode", text="Set Interpolation Mode")

        
def clear_previous_objects(context):
    # Clear previous cameras and paths to allow user to change option without manualy deleting them
    for obj in context.scene.objects:
        if obj.type == 'CAMERA' or obj.name == 'Camera Path':
            bpy.data.objects.remove(obj, do_unlink=True)
        
class CAMERATURN_OT_RunAction(bpy.types.Operator):
    
    bl_idname = "object.rotate_around"
    bl_label = "Turnaround"
    bl_description = "Create camera rotation around selected object"
    bl_options = {'REGISTER', 'UNDO'} 
    
    back_and_forth: bpy.props.BoolProperty(
    name="Back and Forth",
    description="Enable back and forth animation",
    default=False
    )
    
    z_position: bpy.props.FloatProperty(
    name="Z Position",
    description="Z position of the camera relative to the selected object",
    default=3.0,
    min=-10.0,
    max=10.0,
    step=1.0
    )
    
     # type: ignore
    
    def execute(self, context):
          
        #Verify if there is an object selected
        if not context.selected_objects:
            self.report({'ERROR'}, "Select an object before choosing an option!")
            return {'CANCELLED'}
        
        # Clear previous objects
        clear_previous_objects(context)
        
        # Get information from selected object.
        model = context.selected_objects[0]

        # Create path to follow.
        bpy.ops.curve.primitive_bezier_circle_add(radius=1.0, location=(0.0, 0.0, 0.0))        
        camera_path = context.active_object
        camera_path.name = camera_path.data.name = 'Camera Path'
        # Path follows object's scale and location.
        scale_x, scale_y, scale_z = model.scale 
        camera_path.scale = (10*scale_x, 10*scale_y, 10*scale_z)
        location_x, location_y, location_z = model.location
        camera_path.location = (location_x, location_y, location_z+self.z_position)
        
        # Create camera.
        bpy.ops.object.camera_add(location=(0.0, 0.0, 0.0), rotation=(0.0, 0.0, 0.0))
        camera = context.active_object

        # Cache scene.
        scene = context.scene

        # Add follow path constraint.
        follow_path = camera.constraints.new(type='FOLLOW_PATH')
        follow_path.target = camera_path
        follow_path.forward_axis = 'TRACK_NEGATIVE_Z'
        follow_path.up_axis = 'UP_Y'
        follow_path.use_fixed_location = True

        # Add tracking constraint.
        look_at = camera.constraints.new(type='TRACK_TO')
        look_at.target = model
        look_at.track_axis = 'TRACK_NEGATIVE_Z'
        look_at.up_axis = 'UP_Y'

        # Set keyframe for first frame.
        scene.frame_set(scene.frame_start)
        follow_path.offset_factor = 0.0
        follow_path.keyframe_insert(data_path='offset_factor')
        camera_path.keyframe_insert(data_path='location')

        # Set keyframe for middle frame.
        scene.frame_set(scene.frame_start + scene.frame_end // 2)
        follow_path.offset_factor = 0.5
        follow_path.keyframe_insert(data_path='offset_factor')
        camera_path.keyframe_insert(data_path='location')

        # Set keyframe for last frame.
        scene.frame_set(scene.frame_end)
        follow_path.offset_factor = 0.0 if self.back_and_forth else 1.0
        follow_path.keyframe_insert(data_path='offset_factor')
        camera_path.keyframe_insert(data_path='location')
        
        # Store previous selection
        previous_selection = bpy.context.selected_objects

        # Desselect all object
        bpy.ops.object.select_all(action='DESELECT')

        # Select model object again
        if model.name in bpy.data.objects:
            bpy.data.objects[model.name].select_set(True)
            bpy.context.view_layer.objects.active = bpy.data.objects[model.name]
        
        return {'FINISHED'}

class CAMERATURN_OT_SetInterpolationMode(bpy.types.Operator):
    bl_idname = "object.set_interpolation_mode"
    bl_label = "Set Interpolation Mode"
    bl_description = "Set the interpolation mode for camera animation"
    bl_options = {'REGISTER', 'UNDO'}

    interpolation_mode: bpy.props.EnumProperty(
        items=[
            ('LINEAR', 'Linear', 'Linear interpolation'),
            ('BEZIER', 'Bezier', 'Bezier interpolation'),
            # Add more interpolation modes as needed
        ],
        default='LINEAR',
        description="Animation Interpolation Mode"
    ) # type: ignore

    def execute(self, context):
        # Set interpolation mode for keyframes.
        interpolation_mode = self.interpolation_mode
        fcurves = context.scene.camera.animation_data.action.fcurves

        for fcurve in fcurves:
            for kf in fcurve.keyframe_points:
                kf.interpolation = interpolation_mode
                
        return {'FINISHED'}


def register():
    bpy.utils.register_class(MCN_PT_CV_AO)
    bpy.utils.register_class(CAMERATURN_OT_RunAction)
    bpy.utils.register_class(CAMERATURN_OT_SetInterpolationMode)


def unregister():
    bpy.utils.unregister_class(MCN_PT_CV_AO)
    bpy.utils.unregister_class(CAMERATURN_OT_RunAction)
    bpy.utils.unregister_class(CAMERATURN_OT_SetInterpolationMode)

import csv

def importBackground():
    # Chemin vers le fichier CSV
    filepath = 'Chemin/vers/CV_AO_background.csv'  # Modifiez ce chemin selon votre environnement

    vertices = []
    edges = []
    faces = []

    with open(filepath, 'r', newline='', encoding='utf-8') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        next(reader)  # Skip the header
        for row in reader:
            if row[0] == 'v':
                x, y, z = map(float, row[1].split(','))
                vertices.append((x, y, z))
            elif row[0] == 'e':
                vertices_indices = list(map(int, row[1].split(',')))
                edges.append(vertices_indices)
            elif row[0] == 'f':
                vertices_indices = list(map(int, row[1].split(',')))
                faces.append(vertices_indices)

    # Création du mesh
    new_mesh = bpy.data.meshes.new('imported_mesh')
    new_mesh.from_pydata(vertices, edges, faces)
    new_mesh.update()

    # Création de l'objet
    new_object = bpy.data.objects.new('imported_mesh_obj', new_mesh)

    # Ajout de l'objet à la scène
    bpy.context.collection.objects.link(new_object)
    bpy.context.view_layer.objects.active = new_object
    new_object.select_set(True)

    print("Mesh object reconstructed from CSV.")

